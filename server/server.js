const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const config = require("./config");
const users = require('./app/users');
const photos = require('./app/photos');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
  app.use('/users', users);
  app.use('/photos', photos);

  app.listen(config.serverPort, () => {
    console.log(`[Server] started on ${config.serverPort} port`);
  });
});
