const User = require('../models/User');

const auth = (rejectRequestOnFailure = true) => {
  return async (req, res, next) => {
    const token = req.get('Authorization');

    if (!token) {
      if (rejectRequestOnFailure) {
        return res.status(401).send({error: 'Token not provided'});
      } else {
        return next();
      }
    }

    const user = await User.findOne({token});

    if (!user) {
      if (rejectRequestOnFailure) {
        return res.status(401).send({error: 'Token incorrect'});
      } else {
        return next();
      }
    }

    req.user = user;

    next();
  };
};

module.exports = auth;