const path = require('path');

const rootPath = __dirname;

module.exports = {
  serverPort: 8000,
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  dbUrl: 'mongodb://localhost/gallery',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook: {
    appId: '2472013326182142',
    appSecret: '61e501c85493e641f55cfcc78f1ec1be' // insecure!
  }
};
