const mongoose = require('mongoose');
const nanoid = require('nanoid');
const config = require('./config');

const User = require('./models/User');
const Photo = require('./models/Photo');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const [user, fbuser] = await User.create(
    {
      username: 'user',
      password: '123',
      displayName: 'Test User',
      token: nanoid()
    },
    {
      username: 'test_wlefsxu_user@tfbnw.net',
      displayName: 'Test Facebook User',
      facebookId: '109932650251038',
      password: nanoid(),
      token: nanoid()
    }
  );

  await Photo.create(
    {
      title: 'Some image 1',
      image: '_image1.jpeg',
      user: user._id
    },
    {
      title: 'Some image 2',
      image: '_image2.jpeg',
      user: user._id
    },
    {
      title: 'Some image 3',
      image: '_image3.jpg',
      user: user._id
    },
    {
      title: 'Some image 4',
      image: '_image4.jpeg',
      user: fbuser._id
    },
    {
      title: 'Some image 5',
      image: '_image5.jpeg',
      user: fbuser._id
    }
  );

  return connection.close();
};

run().catch(error => {
  console.error('Something wrong happened...', error);
});