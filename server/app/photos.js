const express = require('express');
const auth = require('../middleware/auth');
const upload = require('../middleware/upload');
const Photo = require('../models/Photo');

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    let criteria = {};

    if (req.query.user) {
      criteria = {user: req.query.user};
    }

    const photos = await Photo.find(criteria).populate('user', 'displayName');
    res.send(photos);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.post('/', [auth(), upload.single('image')], (req, res) => {
  const photoData = {
    title: req.body.title,
    user: req.user._id
  };

  if (req.file) {
    photoData.image = req.file.filename;
  }

  const photo = new Photo(photoData);

  photo.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.delete('/:id', auth(), async (req, res) => {
  try {
    const photo = await Photo.findById(req.params.id);

    if (!photo) {
      return res.sendStatus(404);
    }

    if (!photo.user.equals(req.user._id)) {
      return res.sendStatus(403);
    }

    await Photo.deleteOne({_id: photo._id});

    res.send({message: 'Successfully deleted'});
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;