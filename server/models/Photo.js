const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PhtotoSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  }
});

const Photo = mongoose.model('Photo', PhtotoSchema);

module.exports = Photo;