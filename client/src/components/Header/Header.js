import React from 'react';
import {NavLink, withRouter} from "react-router-dom";
import './Header.css';
import Dropdown from "./Dropdown/Dropdown";

const Header = props => {
  const anonMenu = (
    <nav>
      <NavLink to="/register">Register</NavLink>
      <span>or</span>
      <NavLink to="/login">Login</NavLink>
    </nav>
  );

  const userMenu = (
    <Dropdown
      user={props.user}
      displayName={props.user && props.user.displayName}
      avatar={props.user && props.user.avatar}
      logoutUser={props.logoutUser}
    />
  );

  const isCocktailsPage = () => {
    const path = props.location.pathname;
    return path === '/' || path === '/cocktails' || path === '/cocktails/';
  };

  return (
    <div className="Header">
      <NavLink to="/" isActive={isCocktailsPage}>Gallery</NavLink>
      {props.user ? userMenu : anonMenu}
    </div>
  );
};

export default withRouter(Header);
