import React, {Component} from 'react';
import {Link, NavLink} from "react-router-dom";
import {apiURL} from "../../../constants";
import './Dropdown.css';

import noAvatarImg from '../../../assetts/images/noAvatar.png';

class Dropdown extends Component {
  state = {
    showMenu: false
  };

  toggleMenu = () => {
    if (!this.state.showMenu) {
      this.setState({showMenu: true});
      document.addEventListener('click', this.toggleMenu);
    } else {
      this.setState({showMenu: false});
      document.removeEventListener('click', this.toggleMenu);
    }
  };

  getAvatarUrl = avatar => {
    if (avatar.includes('//')) {
      return avatar;
    } else {
      return `${apiURL}/uploads/${avatar}`;
    }
  };

  render() {
    return (
      <div className="Dropdown">
        <button
          onClick={this.toggleMenu}
          className={"Dropdown-btn " + (this.state.showMenu ? "arrowUp" : "arrowDown")}
        >
          <img src={this.props.avatar ? this.getAvatarUrl(this.props.avatar) : noAvatarImg} alt=""/>
          <span>{this.props.displayName}</span>
        </button>
        <div
          className="Dropdown-menu"
          style={{display: this.state.showMenu ? 'block' : 'none'}}
        >
          <nav>
            <NavLink to={`/gallery/${this.props.user._id}`} exact>My gallery</NavLink>
            <Link to="/" onClick={this.props.logoutUser}>Logout</Link>
          </nav>
        </div>
      </div>
    );
  }
}

export default Dropdown;