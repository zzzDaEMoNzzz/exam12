import React from 'react';

const TextArea = props => {
  return (
    <textarea
      id={props.propertyName}
      name={props.propertyName}
      value={props.value}
      onChange={props.onChange}
      required={props.required}
      rows="5"
    />
  );
};

export default TextArea;
