import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import './FormElement.css';
import Select from "./Select/Select";
import TextArea from "./TextArea/TextArea";
import Input from "./Input/Input";

class FormElement extends Component {
  state = {
    errorMessage: null
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.error !== prevProps.error) {
      this.setState({errorMessage: this.props.error});
    }
  }

  onInputChange = event => {
    this.setState({errorMessage: null});

    this.props.onChange(event);
  };

  render() {
    let formElement = null;

    switch (this.props.type) {
      case 'select':
        formElement = <Select {...this.props}/>;
        break;
      case 'textarea':
        formElement = <TextArea {...this.props}/>;
        break;
      case 'text':
      case 'password':
      case 'file':
      default:
        formElement = <Input {...this.props} error={this.state.errorMessage} onChange={this.onInputChange}/>;
        break;
    }

    return (
      <Fragment>
        <label
          htmlFor={this.props.propertyName}
          className={this.props.required ? 'FormElement-requiredIcon' : ''}
          title={this.props.required ? 'required' : ''}
        >
          {this.props.title}
          {this.state.errorMessage && (
            <span className="FormElement-errorMessage">{this.state.errorMessage}</span>
          )}
        </label>
        {formElement}
      </Fragment>
    );
  }
}

FormElement.propTypes = {
  propertyName: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  onChange: PropTypes.func,
  onChangeFile: PropTypes.func,
  type: PropTypes.oneOf(['text', 'password', 'textarea', 'file', 'select']),
  list: PropTypes.arrayOf(PropTypes.object),
  loading: PropTypes.bool,
  reference: PropTypes.object,
  error: PropTypes.string,
  required: PropTypes.bool,
};

FormElement.defaultProps = {
  type: 'text'
};

export default FormElement;