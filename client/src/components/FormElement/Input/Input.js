import React from 'react';

const Input = props => {
  const inputProps = {
    className: props.error ? 'FormElement-inputError' : '',
    type: props.type,
    id: props.propertyName,
    name: props.propertyName,
    onChange: props.type === 'file' ? props.onChangeFile : props.onChange,
    required: props.required
  };

  if (props.type !== 'file') {
    inputProps.value = props.value;
  }

  return (
    <input {...inputProps}/>
  );
};

export default Input;
