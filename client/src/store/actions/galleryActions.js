import axios from '../../axios-api';
import {push} from 'connected-react-router';
import {NotificationManager} from 'react-notifications';

export const GET_GALLERY_SUCCESS = 'GET_GALLERY_SUCCESS';
const getGallerySuccess = photos => ({type: GET_GALLERY_SUCCESS, photos});

export const getGallery = userId => {
  return dispatch => {
    let url = '/photos';

    if (userId) {
      url += `/?user=${userId}`;
    }

    axios.get(url).then(
      response => dispatch(getGallerySuccess(response.data)),
      () => NotificationManager.error('An error has occurred when loading data')
    )
  };
};

export const addPhoto = data => {
  return dispatch => {
    axios.post('/photos', data).then(
      () => {
        NotificationManager.success('Photo successfully added');
        dispatch(push('/'));
      },
      error => {
        try {
          const errors = error.response.data.errors;

          const errorMessages = Object.values(errors).map(error => error.message);

          NotificationManager.error(errorMessages.join(' '));
        } catch (e) {
          NotificationManager.error('An error occurred while adding a photo');
        }
      }
    );
  };
};

export const deletePhoto = id => {
  return dispatch => {
    return axios.delete(`/photos/${id}`).then(
      () => NotificationManager.success('Successfully deleted'),
      () => NotificationManager.error('An error occurred while deleting a photo')
    )
  };
};