import axios from '../../axios-api';
import {push} from 'connected-react-router';
import {NotificationManager} from 'react-notifications';

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, user});
const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

export const registerUser = userData => {
  return dispatch => {
    return axios.post('/users', userData).then(
      response => {
        dispatch(registerUserSuccess(response.data));
        dispatch(push('/'));
      },
      error => {
        if (error.response) {
          dispatch(registerUserFailure(error.response.data))
        } else {
          dispatch(registerUserFailure({global: 'No connection'}))
        }
      }
    )
  }
};

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

export const loginUser = userData => {
  return dispatch => {
    return axios.post('/users/sessions', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data));
        dispatch(push('/'));
      },
      error => {
        if (error.response) {
          dispatch(loginUserFailure(error.response.data))
        } else {
          dispatch(loginUserFailure({global: 'No connection'}))
        }
      }
    );
  }
};

export const LOGOUT_USER = 'LOGOUT_USER';

export const logoutUser = () => {
  return dispatch => {
    return axios.delete('/users/sessions').then(
      () => {
        dispatch({type: LOGOUT_USER});
        NotificationManager.success('Logged out!');
      },
      () => {
        NotificationManager.error('Could not logout');
      }
    );
  };
};

export const checkAuth = () => {
  return dispatch => {
    axios.get('/users/sessions').then(
      null,
      () => dispatch({type: LOGOUT_USER})
    );
  };
};

export const facebookLogin = userData => {
  return dispatch => {
    return axios.post('/users/facebookLogin', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data));
        NotificationManager.success('Logged in via Facebook');
        dispatch(push('/'));
      },
      () => {
        dispatch(loginUserFailure('Login via Facebook failed'));
      }
    )
  };
};