import {GET_GALLERY_SUCCESS} from "../actions/galleryActions";

const initialState = {
  photos: []
};

const galleryReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_GALLERY_SUCCESS:
      return {...state, photos: action.photos};
    default:
      return state;
  }
};

export default galleryReducer;