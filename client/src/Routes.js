import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Gallery from "./containers/Gallery/Gallery";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddPhoto from "./containers/AddPhoto/AddPhoto";

const ProtectedRoute = ({isAllowed, ...props}) => {
  return isAllowed ? <Route {...props}/> : <Redirect to="/login"/>;
};

const Routes = ({user}) => {
  return (
    <Switch>
      <Route path="/" exact component={Gallery}/>
      <Route path="/gallery" exact component={Gallery}/>
      <ProtectedRoute
        isAllowed={!!user}
        path="/gallery/add"
        exact
        component={AddPhoto}
      />
      <ProtectedRoute
        isAllowed={!!user}
        path="/gallery/:user"
        exact
        component={Gallery}
      />
      <Route path="/register" exact component={Register}/>
      <Route path="/login" exact component={Login}/>
      <Route render={() => <h2>Page not found...</h2>}/>
    </Switch>
  );
};

export default Routes;
