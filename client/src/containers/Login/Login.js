import React, {Component} from 'react';
import {connect} from "react-redux";
import {NotificationManager} from 'react-notifications';
import './Login.css';
import 'react-notifications/lib/notifications.css';
import FormElement from "../../components/FormElement/FormElement";
import {loginUser} from "../../store/actions/usersActions";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

class Login extends Component {
  state = {
    username: '',
    password: ''
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.error !== this.props.error) {
      if (this.props.error && this.props.error.error) {
        NotificationManager.error(this.props.error.error, 'Authentication error!', 5000);
      }
    }
  }

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  submitFormHandler = event => {
    event.preventDefault();

    this.props.loginUser({...this.state});
  };

  render() {
    return (
      <div>
        <h2>Login</h2>
        <div className="Login-socials">
          <FacebookLogin/>
        </div>
        <form className="Login" onSubmit={this.submitFormHandler}>
          <FormElement
            title="Username"
            propertyName="username"
            type="text"
            value={this.state.username}
            onChange={this.inputChangeHandler}
            required
          />
          <FormElement
            title="Password"
            propertyName="password"
            type="password"
            value={this.state.password}
            onChange={this.inputChangeHandler}
            required
          />
          <button>Login</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
  loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);