import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import Modal from 'react-modal';
import {apiURL} from "../../constants";
import {deletePhoto, getGallery} from "../../store/actions/galleryActions";
import './Gallery.css';

class Gallery extends Component {
  state = {
    modalImage: null
  };

  isUserGallery = this.props.match.params && this.props.match.params.user;

  componentDidMount() {
    this.props.getGallery(this.isUserGallery);
  }

  hideModal = () => {
    this.setState({
      modalImage: null
    });
  };

  showModal = url => {
    this.setState({
      modalImage: url
    });
  };

  deletePhoto = (event, id) => {
    event.stopPropagation();

    if (window.confirm('Are you sure you want to delete this photo?')) {
      this.props.deletePhoto(id).then(() => {
        this.props.getGallery(this.isUserGallery);
      });
    }
  };

  render() {
    if (this.props.photos.length === 0) {
      return <h2>This gallery is empty!</h2>;
    }

    return (
      <Fragment>
        {this.isUserGallery && (
          <h2 className="Gallery-header">
            {this.props.photos[0].user.displayName}'s gallery
            <Link to="/gallery/add">Add new photo</Link>
          </h2>
        )}

        <div className="Gallery">
          <Modal
            isOpen={!!this.state.modalImage}
            className="Gallery-modal"
            overlayClassName="Gallery-modalOverlay"
          >
            <button onClick={this.hideModal}>X</button>
            <img src={this.state.modalImage} alt=""/>
          </Modal>

          {this.props.photos.map(photo => (
            <div
              key={photo._id}
              className="Gallery-card"
              onClick={() => this.showModal(`${apiURL}/uploads/${photo.image}`)}
            >
              <div className="Gallery-imageWrapper">
                <img src={`${apiURL}/uploads/${photo.image}`} alt=""/>
              </div>
              <div className="Gallery-cardTitle">
                <h4>{photo.title}</h4>
                {!this.isUserGallery && (
                  <p><Link to={`/gallery/${photo.user._id}`}>by {photo.user.displayName}</Link></p>
                )}
              </div>
              {!!this.isUserGallery && this.isUserGallery === this.props.user._id && (
                <button
                  className="Gallery-cardDeleteBtn"
                  onClick={event => this.deletePhoto(event, photo._id)}
                >
                  X
                </button>
              )}
            </div>
          ))}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  photos: state.gallery.photos,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  getGallery: userId => dispatch(getGallery(userId)),
  deletePhoto: id => dispatch(deletePhoto(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);