import React, {Component} from 'react';
import {connect} from "react-redux";
import './Register.css';
import {registerUser} from "../../store/actions/usersActions";
import FormElement from "../../components/FormElement/FormElement";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

class Register extends Component {
  state = {
    username: '',
    password: '',
    displayName: '',
    avatar: null
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.registerUser(formData);
  };

  getFieldError = fieldName => {
    return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
  };

  render() {
    return (
      <div>
        <h2>Register new user</h2>
        <div className="Register-socials">
          <FacebookLogin/>
        </div>
        <form className="Register" onSubmit={this.submitFormHandler}>
          <FormElement
            title="Username"
            propertyName="username"
            type="text"
            value={this.state.username}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('username')}
            required
          />
          <FormElement
            title="Password"
            propertyName="password"
            type="password"
            value={this.state.password}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('password')}
            required
          />
          <FormElement
            title="Display name"
            propertyName="displayName"
            type="text"
            value={this.state.displayName}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('displayName')}
            required
          />
          <FormElement
            title="Avatar"
            propertyName="avatar"
            type="file"
            onChangeFile={this.fileChangeHandler}
            error={this.getFieldError('avatar')}
          />
          <button>Register</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
  registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);