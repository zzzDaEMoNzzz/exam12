import React, {Component, Fragment} from 'react';
import './AddPhoto.css';
import {connect} from "react-redux";
import {addPhoto} from "../../store/actions/galleryActions";

class AddPhoto extends Component {
  state = {
    title: '',
    image: null
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      image: event.target.files[0]
    });
  };

  onSubmit = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key,  this.state[key]);
    });

    this.props.addPhoto(formData);
  };

  render() {
    return (
      <Fragment>
        <h2>Add new photo</h2>
        <form className="AddPhoto" onSubmit={this.onSubmit}>
          <label htmlFor="title">Title</label>
          <input
            type="text"
            id="title"
            name="title"
            value={this.state.title}
            onChange={this.inputChangeHandler}
            required
          />
          <label htmlFor="image">Image</label>
          <input
            type="file"
            id="image"
            onChange={this.fileChangeHandler}
            required
          />
          <button>Create photo</button>
        </form>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addPhoto: data => dispatch(addPhoto(data))
});

export default connect(null, mapDispatchToProps)(AddPhoto);